public class Persona {
    protected String nome;
    protected String cognome;
    protected String indirizzo;
    protected String telefono;
    protected int eta;

    public Persona (String n, String c, String i, String t, int e) {
        this.nome = n;
        this.cognome = c;
        this.indirizzo = i;
        this.telefono = t;
        this.eta = e;
    }

    public String personaToLine () {
        return nome+";"+cognome+";"+indirizzo+";"+telefono+";"+eta;
    }
}

