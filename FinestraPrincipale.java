import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FinestraPrincipale {
    static RubricaManager rubrica;
    JFrame frame;
    JTable table;

    FinestraPrincipale() {
        rubrica = new RubricaManager();
        frame = new JFrame("Rubrica");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel jp1 = new JPanel();
        JPanel jp2 = new JPanel();

        table = new JTable();
        String data[][] = rubrica.getElenco();
        String column[] = {"Nome", "Cognome", "Telefono"};
        TableModel model = new DefaultTableModel(data, column) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ((DefaultTableModel)model).fireTableDataChanged();
        table.setModel(model);
        JScrollPane sp = new JScrollPane(table);

        JButton newb = new JButton("Nuovo");
        newb.addActionListener(new NewListener());
        JButton editb = new JButton("Modifica");
        editb.addActionListener(new EditListener());
        JButton delb = new JButton("Elimina");
        delb.addActionListener(new DelListener());

        jp1.add(sp);
        jp2.add(newb);
        jp2.add(editb);
        jp2.add(delb);

        frame.getContentPane().add(new JLabel("Rubrica for webturing.net"), BorderLayout.NORTH);
        frame.getContentPane().add(jp1, BorderLayout.CENTER);
        frame.getContentPane().add(jp2, BorderLayout.SOUTH);
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new FinestraPrincipale();
    }

    class NewListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            new FinestraEditorPersona(null, rubrica, table, -1);
        }
    }

    class EditListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            int row = table.getSelectedRow();
            if (row == -1) {
                JOptionPane.showMessageDialog(null,
                        "Nessun contatto selezionato",
                        "Modifica",JOptionPane.ERROR_MESSAGE);
            }
            else {
                int res = JOptionPane.showConfirmDialog(null,
                        "Modificare la persona "+rubrica.getNomeAt(row)+"?",
                        "Modifica",JOptionPane.YES_NO_OPTION);
                if (res == 0){ //YES
                    new FinestraEditorPersona(rubrica.getPersonaAt(row), rubrica, table, row);
                }
            }
        }
    }

    class DelListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            int row = table.getSelectedRow();
            if (row == -1) {
                JOptionPane.showMessageDialog(null,
                        "Nessun contatto selezionato",
                        "Elimina",JOptionPane.ERROR_MESSAGE);
            }
            else {
                int res = JOptionPane.showConfirmDialog(null,
                        "Elimniare la persona "+rubrica.getNomeAt(row)+"?",
                        "Elimina",JOptionPane.YES_NO_OPTION);
                if (res == 0){ //YES
                    rubrica.removeAt(row);
                    ((DefaultTableModel)table.getModel()).removeRow(row);
                }
            }
        }
    }
}