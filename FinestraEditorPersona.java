import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

public class FinestraEditorPersona {
    RubricaManager rubrica;
    JTable table;
    JFrame frame;
    JTextField nt,ct,it,tt,et;
    int row;
    FinestraEditorPersona (Persona p, RubricaManager r, JTable t, int w) {
        frame = new JFrame("Editor Persona");
        rubrica = r;
        table = t;
        row = w;
        JPanel jp = new JPanel(new GridLayout(0,2));

        JLabel nl = new JLabel("Nome: ");
        JLabel cl = new JLabel("Cognome: ");
        JLabel il = new JLabel("Indirizzo: ");
        JLabel tl = new JLabel("Telefono: ");
        JLabel el = new JLabel("Età: ");

        if ( row == -1 ) {
            nt = new JTextField();
            ct = new JTextField();
            it = new JTextField();
            tt = new JTextField();
            et = new JTextField();
        }
        else {
            nt = new JTextField(p.nome);
            ct = new JTextField(p.cognome);
            it = new JTextField(p.indirizzo);
            tt = new JTextField(p.telefono);
            et = new JTextField(p.eta+"");
        }
        JButton saveb = new JButton("Salva");
        saveb.addActionListener(new SaveListener());
        JButton cancb = new JButton("Annulla");
        cancb.addActionListener((new CancListener()));
        jp.add(nl);
        jp.add(nt);
        jp.add(cl);
        jp.add(ct);
        jp.add(il);
        jp.add(it);
        jp.add(tl);
        jp.add(tt);
        jp.add(el);
        jp.add(et);
        jp.add(saveb);
        jp.add(cancb);

        frame.add(jp);
        frame.pack();
        frame.setVisible(true);
    }
    class SaveListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            Persona p = new Persona (
                    nt.getText(),
                    ct.getText(),
                    it.getText(),
                    tt.getText(),
                    Integer.parseInt(et.getText())
            );
            if (row == -1) {
                rubrica.addPersona(p);
                ((DefaultTableModel) table.getModel()).addRow(new String[]{p.nome,p.cognome,p.telefono});
            }
            else {
                table.setValueAt(p.nome,row,0);
                table.setValueAt(p.cognome,row,1);
                table.setValueAt(p.telefono,row,2);
                table.updateUI();
                rubrica.changePersona(row, p);
            }
            frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
        }
    }

    class CancListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
        }
    }
}
