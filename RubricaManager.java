import java.io.*;
import java.util.ArrayList;

public class RubricaManager {
    private ArrayList<Persona> elenco;
    private final String filepath = "informazioni.txt";
    private final String tmpfilepath = "tmpinformazioni.txt";
    private final int maxrow = 3;

    public RubricaManager() {
        elenco = new ArrayList<Persona>();
        try {
            File f = new File(filepath);
            if (!f.exists())
                f.createNewFile();
            FileReader fr = new FileReader(f);
            BufferedReader buff = new BufferedReader(fr);
            String templine = buff.readLine();

            while (templine != null) {
                String[] svar = templine.split(";");
                Persona p = new Persona (
                                svar[0],
                                svar[1],
                                svar[2],
                                svar[3],
                                Integer.parseInt(svar[4])
                    );
                elenco.add(p);
                templine = buff.readLine();
            }
            fr.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String[][] getElenco() {
        String[][] s = new String[elenco.size()][maxrow];
        for (int i = 0 ; i < elenco.size(); i++) {
            Persona tp = elenco.get(i);
            s[i][0] = tp.nome;
            s[i][1] = tp.cognome;
            s[i][2] = tp.telefono;
        }
        return s;
    }

    public Persona getPersonaAt(int i) {
        return elenco.get(i);
    }

    public String getNomeAt(int i) {
        return elenco.get(i).nome+" "+elenco.get(i).cognome;
    }

    public void addPersona(Persona p) {
        elenco.add(p);
        try {
            File inputFile = new File(filepath);
            BufferedWriter bw = new BufferedWriter(new FileWriter(inputFile,true));
            bw.append(p.personaToLine()+"\n");
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public void changePersona (int row, Persona p) {
        elenco.remove(row);
        elenco.add(row,p);
        try {
            File inputFile = new File(filepath);
            File tempFile = new File(tmpfilepath);
            BufferedReader reader = new BufferedReader(new FileReader(inputFile));
            BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
            String line;
            int i = 0;
            while ((line = reader.readLine()) != null) {
                if (row == i) {
                    writer.write(p.personaToLine() + System.getProperty("line.separator"));
                }
                else {
                    writer.write(line + System.getProperty("line.separator"));
                }
                i++;
            }

            writer.close();
            reader.close();
            tempFile.renameTo(inputFile);
            File f = new File (tmpfilepath);
            f.delete();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void removeAt(int row) {
        elenco.remove(row);
        try {
            File inputFile = new File(filepath);
            File tempFile = new File(tmpfilepath);
            BufferedReader reader = new BufferedReader(new FileReader(inputFile));
            BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
            String line;
            int i = 0;
            while ((line = reader.readLine()) != null) {
                if (row != i) {
                    writer.write(line + System.getProperty("line.separator"));
                }
                i++;
            }

            writer.close();
            reader.close();
            tempFile.renameTo(inputFile);
            File f = new File (tmpfilepath);
            f.delete();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

